/** ===========================================================================
* @file h265dec_testapp.c
*
* @brief This file defines test application for PathPartner's
*        H265 decoder.
*
* ============================================================================
*
* Copyright � PathPartner Technology, 2012-2015
*
* This material,including documentation and  any related computer programs,is
* protected by copyright controlled by PathPartner Technology. All rights are
* reserved.
*
* ============================================================================
*
* <b> REVISION HISTORY </b>
* @n  ================  @n
* @version 0.1 : 02-July-2012 : Sunil K : Initial Code
* @version 0.2 : 14-Mar-2013  : Nandu : Code changes for multi threading
* @version 0.3 : 9 -May-2014  : Akshatha: Changes done dynamic threading
* $RevLog$
*
*=============================================================================
*/

/*******************************************************************************
*   INCLUDE FILES
*******************************************************************************/

#include "exp_pp_h265decoder.h"
#include "h265vdec_pp_allocutils.h"
#include "h265vdec_pp_buffer_manager.h"
#include "h265vdec_pp_parsecfg.h"

#ifdef ARM_PROFILE
#include <sys/time.h>
#endif

/*******************************************************************************
*   MACROS 
*******************************************************************************/

#define Main() main(int argc, char *argv[])
#define INFILE argv[1]
#define OUTFILE argv[2]
/* DBP size */
#define H265_DPB_SIZE_PROFILE_3_0 (98*1024*1024)
/* Macro to indicate maximum I/O file length */
#define MAX_FILENAME_LEN (256)
/* Macro to indicate maximum number of cores/threads */
#define MAX_NUM_THREADS (4)
/* Data packet size */
#define H265_PACKET_SIZE (8*1024*1024)
/* YUV dump enable*/
#define FILE_DUMP
/* Output low dealy buffer */
#define LOW_DELAY_OUTBUFF   (2088960)
/*#define LOW_DELAY_INTERFACE_SLICEMODE*/


#ifdef ARM_PROFILE
#include <sys/time.h>
unsigned long long process_time;
unsigned long long avg_process_time;
unsigned long long peak_process_time;
unsigned long long total_time;
float frame_rate;
unsigned int nFrmNo;
struct timeval tProcessSt, tProcessEd;
#endif

#ifdef LOW_DELAY_INTERFACE_SLICEMODE
tPPu32 nFileRead_Sync = 0;
tPPu32 nNoBytes_Sync = 0;
tPPu32 nByteRead_Sync = 0;
tPPu32 nByteLeft_Sync = 0;
tPPu32 nBufFilled_Sync = 0;
/* Size of slice data NAL units  */
tPPu32 Bytes[100] = {1522, 2457, 2825, 2135, 1718, 100, 158, 176, 140,
                    187, 20, 22, 30, 25, 28};
tPPu32 Bytes_frames[100] = {10657, 761, 126};
tPPu32 inputSync_Counter = 0;
#endif

#ifndef _WIN32
    pthread_t thread[MAX_NUM_THREADS];    /* Thread identifiers */
#endif

/* Arrays for input and output filenames */
tPPi8  inFile[MAX_FILENAME_LEN];
tPPi8  outFile[MAX_FILENAME_LEN];
tPPi8  nTestCasesFile[FILE_NAME_SIZE];
tPPi8  configFile[FILE_NAME_SIZE];
tPPi8  aErTraceFile[FILE_NAME_SIZE + 4];
/* Input Bitstream array*/
tPPu8  nBitStream[H265_PACKET_SIZE];

 /* File pointer variable for test and reference file */
FILE *fpDataSync   = NULL;
FILE *fErTraceFile = NULL;
FILE *fpInput;
FILE *fpOutput;
FILE *fpRef;
FILE  *fTestCasesFile;   

/* Global structures */
tPP_H265_CreateParams        h265_CreateParams;
tPP_H265_DynamicParams       h265_DynamicParams;
tPP_H265_ConfigParams        h265_ConfigParams;
tPP_H265_DataSyncDesc        gInputDataSyncParams;
tPPBaseDecoder              *h265dec = NULL;
tPPYUVPlanarDisplayFrame     pOutBuf[PP_H265VDEC_MAX_REF_FRAMES + 1];
tPPInput_BitStream           Input_buff;

/* Global variables */
tPPu32 nRowNum;
tPPu16 nDataSyncIntrCntr;

/* Buffers for output low delay */
tPPu8  ybuf[LOW_DELAY_OUTBUFF];
tPPu8  cbbuf[LOW_DELAY_OUTBUFF];
tPPu8  crbuf[LOW_DELAY_OUTBUFF];
tPPu8  nCompareOutput = 0;
tPPu8  nRefBuf[LOW_DELAY_OUTBUFF];

/* Volatile variables */
volatile tPPi32 gCurrHeight = 0;
volatile tPPu32 nInitCompleted = 0;
volatile tPPi32 nDecFrmNo = 0;
volatile tPPi32 nDispFrameNo = 0;
volatile long completed_testapp;
volatile tPPi32 nFrameDecodeStart ;
volatile tPPi32 nFrameDecodeEnd;
volatile tPPi32 nTerminateStatus;
volatile tPPu32 nFrameSync = 0;
volatile tPPu32 nStatus = 0;
volatile tPPu32 nSlaveExit = 0;

#ifdef ARM_PROFILE
#define PROF_BUF_SIZE (10000)
tPPu64  PeakDecodeTime( tPPu64  *pTimerBuffer, tPPu32  nNumFrame);
#endif

/*******************************************************************************
*   FUNCTION PROTOTYPES 
*******************************************************************************/

static tPPu32 WriteOutputFrame(tPPYUVPlanarDisplayFrame *pOutFrame,
                               FILE  *fpOutput);

static tPPi32 CompareOutputFrame(tPPYUVPlanarDisplayFrame *pOutFrame,
                               FILE  *fpRef);

static void gGetProfileNums();

static tPPi32 sPP_H265_MultiThreadInit(tPPMultiThreadParams *nMultiThreadParam,
                                       tPPu32 nThreadID, tPPu32 nNumThreads);
void *DecodeTask(void *thread_args);

tPPResult gPP_PPL_AtomicAdd8(tPPu8 *ptr, tPPu8 val);

void gH265_putDataSync(tPP_H265_DataSyncDesc *pDataSyncParams);

void gH265_getDataSync(tPP_H265_DataSyncDesc *pDataSyncParams);

tPPResult gH265_Error_Report(FILE *fTraceFile, tPPi32 nError);

/*******************************************************************************
*   FUNCTION DEFINITONS 
*******************************************************************************/

/**
******************************************************************************
* @fn main(tPPi32 argc, tPPi8 *argv[])
*
* @brief Main function calling H265 decoder APIs to decode H265 bitstream
*        from file to YUV data into another file
*        Usage: decoder.exe arg1 arg2
*               arg1 - Name of the file containing H265 bitstream to be decoded
*               arg2 - Name of the file to write output YUV data
*
* @param argc [IN] Argument count
* @param argv [IN] Argument vector
*
* @return None
******************************************************************************
*/

int main(tPPi32 argc, tPPi8 *argv[])
{
    tPPi32 nThreads = MAX_NUM_THREADS;
    tPPi32 taskid[4]={0};      /* Id numbers for each thread */
    tPPi32 i;
#ifndef _WIN32
    int *pThreadRetVal[(MAX_NUM_THREADS -1)] ;
#endif
    /* Initialize global variables */
    nFrameDecodeStart = 0;
    completed_testapp = 0;
    /* basic initializations */
    fpInput    = NULL;
    fpOutput   = NULL;
    h265_CreateParams.nMaxWidth = PP_H265_MAX_PIC_WIDTH;
    h265_CreateParams.nMaxHeight = PP_H265_MAX_PIC_HEIGHT;
    h265_CreateParams.nNumThreads = nThreads;

#ifdef BATCH_MODE
    strcpy((char *)configFile,"Testparams.cfg");
#else
#ifdef _WIN32
    strcpy((char *)configFile,
        "..\\..\\..\\Test\\TestVecs\\Config\\Testparams.cfg");
#else
    strcpy((char *)configFile,"Testparams.cfg");
#endif
#endif

    /*------------------------------------------------------------------------*/
    /*  Open Test Config File                                                 */
    /*------------------------------------------------------------------------*/
    fTestCasesFile = fopen((const char *)configFile,"rb");

    if(fTestCasesFile == NULL)
    {
        printf("\n Unable to open Config List  %s\n",
            (char *)configFile);
        fflush(stdout);
        return EC_PP_FILE_OPEN_FAIL;
    }

    if (readparamfile(configFile) < 0)
    {
        printf("Syntax Error in %s\n", configFile);
        fflush(stdout);
        printf("Exiting for this configuration...\n");
        fflush(stdout);
        fclose(fTestCasesFile);
        return EC_PP_FILE_READ_FAIL;
    }
    /* Open input file */
    fpInput = fopen((char *)h265_ConfigParams.nInputFile,"rb");
    if(fpInput == NULL)
    {
        printf("Unable to open Input file.\n");
        fflush(stdout);
        fclose(fTestCasesFile);
        return EC_PP_FILE_OPEN_FAIL;
    }
    /* Open ref file */
    fpRef = fopen((char *)h265_ConfigParams.nRefFile,"rb");
    if(fpRef != NULL)
    {
        nCompareOutput = 1;
    }

    /* Open output file */
    fpOutput = fopen(h265_ConfigParams.nOutputFile,"wb");
    if(fpOutput == NULL)
    {
        printf("Unable to open Output file.\n");
        fflush(stdout);
        fclose(fTestCasesFile);
        fclose(fpInput);
        if(fpRef)
        {
            fclose(fpRef);
        }
        return EC_PP_FILE_OPEN_FAIL;
    }
    if(h265_ConfigParams.nOutputLowDelayMode == 1)
    {
        fpDataSync = fopen("Dump.yuv","wb");
         if(fpDataSync == NULL)
         {
             printf("Unable to output dump file.\n");
             fflush(stdout);
             fclose(fTestCasesFile);
             fclose(fpInput);
             fclose(fpOutput);
             if(fpRef)
             {
                 fclose(fpRef);
             }
             return EC_PP_FILE_OPEN_FAIL;
         }

    }
#ifdef DUMP_ERROR
    /*----------------------------------------------------------------*/
    /*    Trace file to write the decoded error details               */
    /*----------------------------------------------------------------*/
    strcpy((char *)aErTraceFile, (const char *)h265_ConfigParams.nOutputFile);
    strcat((char *)aErTraceFile, ".txt");
    fErTraceFile = fopen((const char *)aErTraceFile, "w");

    if((fErTraceFile)== NULL)
    {
        printf("\n Unable to open trace file to write error \
               details %s\n",(char *)fErTraceFile);
        fflush(stdout);
        fclose(fTestCasesFile);
        fclose(fpInput);
        fclose(fpOutput);
        if(fpRef)
        {
            fclose(fpRef);
        }
        if(fpDataSync)
        {
            fclose(fpDataSync);
        }
        return EC_PP_FILE_OPEN_FAIL;
    }
    else
    {
        fprintf(fErTraceFile, "Config File   : %s\n",(char *)configFile);
    }
#endif
    /*Copying Config params to create params*/
    {
      h265_CreateParams.nNumThreads         =
          h265_ConfigParams.nNumThreads;
      h265_CreateParams.nMaxWidth           =
          h265_ConfigParams.nMaxWidth;
      h265_CreateParams.nMaxHeight          =
          h265_ConfigParams.nMaxHeight;
      h265_CreateParams.nInputLowDelayMode  =
          h265_ConfigParams.nInputLowDelayMode;
      h265_CreateParams.nOutputLowDelayMode =
          h265_ConfigParams.nOutputLowDelayMode;
      h265_CreateParams.nNumCTURows =
          h265_ConfigParams.nNumCTURows;

      if(h265_ConfigParams.nMetadataType == 1)
      {
          h265_CreateParams.nMetadataType = PP_H265_METADATA_SEI_DATA;
      }
      else if(h265_ConfigParams.nMetadataType == 2)
      {
          h265_CreateParams.nMetadataType = PP_H265_METADATA_VUI_DATA;
      }
      else
      {
          if(h265_ConfigParams.nMetadataType == 3)
          {
              h265_CreateParams.nMetadataType =\
                  PP_H265_METADATA_SEI_DATA|PP_H265_METADATA_VUI_DATA;
          }
          else
          {
              h265_CreateParams.nMetadataType = h265_ConfigParams.nMetadataType;
          }
      }

      h265_DynamicParams.nDecodeHeader = h265_ConfigParams.nDecodeHeader;

      if(h265_ConfigParams.nOutputLowDelayMode == PP_H265_NUMCTUROWS)
      {
          h265_DynamicParams.fOutputLowDelayFxn
              = (gH265_DataSyncPutFxn)gH265_putDataSync;
      }
      else
      {
          h265_DynamicParams.fOutputLowDelayFxn = NULL;
      }

      if(h265_ConfigParams.nInputLowDelayMode == PP_H265_SLICEMODE)
      {
          h265_DynamicParams.fInputLowDelayFxn
              = (gH265_DataSyncPutFxn)gH265_getDataSync;
      }
      else
      {
          h265_DynamicParams.fInputLowDelayFxn  = NULL;
      }
    }
    nThreads = h265_CreateParams.nNumThreads ;

#ifndef _WIN32
    tPPi32 nRet;
    for(i = 0; (i < nThreads); i++)
    {
        taskid[i] = i;
    }

    for(i = 1; i < nThreads; i++)
    {
        /* Create a thread with its argument in taskid[i] */
        nRet = pthread_create(&thread[i], NULL, DecodeTask,
            &taskid[i]);
        if (nRet)
        {
            /* Check for errors */
            printf("ERROR; return code from pthread_create() is %d\n", 
                    nRet);
            fflush(stdout);
        }
    }
#else
    nThreads = h265_CreateParams.nNumThreads = 1;
    for(i = 0; (i < nThreads); i++)
    {
        taskid[i] = i;
    }
#endif

    DecodeTask(&taskid[0]);

    if(fTestCasesFile)
    {
         fclose(fTestCasesFile);
    }
    if(fErTraceFile)
    {
        fclose(fErTraceFile);
    }
    if(fpInput)
    {
        fclose(fpInput);
    }
    if(fpOutput)
    {
        fclose(fpOutput);
    }
    if(fpRef)
    {
        fclose(fpRef);
    }
    if(fpDataSync)
    {
        fclose(fpDataSync);
    }
    printf("Decode completed\n");
    fflush(stdout);
#ifndef _WIN32
    for(i = 0; i < (nThreads - 1); i++)
    {    
        pthread_join(thread[(i + 1)] ,(void **)&pThreadRetVal[i]); 
    }

#endif
    return 0;
}
/**
********************************************************************************
 * @fn     void *DecodeTask(void *thread_args)
 *
 * @brief   Task processed by threads
 *
 * @param   thread_args [IN]  thread arguments
 *
 * @return  void
********************************************************************************
*/
void *DecodeTask(void *thread_args)
{
    tPPu32 nFileRead;
    tPPu32 nNoBytes;
    tPPu32 nByteRead ;
    tPPu32 nByteLeft;
    tPPu32 nIndex;
    tPPu32 nBytesDec;
    tPPu32 nBufFilled = 0;
    tPPu32 nBytesConsumed = 0;
    tPPu32 nTOTAL_FRAME_SIZE;
    tPPu32 nLumSize;
    tPPu32 nCbSize;
    tPPu32 nCrSize;
    tPPi32 nRetVal   = 0;
    tPPi32 nInitDone = EC_PP_FAILURE;
    tPPi32 nAllocDone = EC_PP_FAILURE;
    /*MultiThread*/
    tPPMultiThreadParams nMultiThreadParam;
    tPPu32 nCurrThreadID = 0;
    tPPi32 nRetV;

#ifdef ARM_PROFILE
    tPPu64  *pTimerBuffer;
#endif
    tPPu32 nNumMemTabEntries = 0;
    tPPQueryMemRecords ReqMemTab[PP_H265DEC_MAX_MEMTAB];
    BUFFMGR_buffEleHandle H265_dec_BuffEle = NULL;
    tPPInFrame_Buff  H265_dec_InBuff;
    tPPOutFrame_Buff H265_dec_OutBuff;
    tPPDecParam_Status apVal ;

    tPPi32 *thread_ptr = thread_args;
    tPPi32 thread_no = *thread_ptr;
    tPPi32 nResult = 0;

#ifdef ARM_PROFILE
    extern struct timeval tProcessSt, tProcessEd;
#endif
    nByteRead  = 0;
    nByteLeft  = 0;
    nBytesDec  = 0;
    Input_buff.nBitStream = nBitStream;

#ifdef ARM_PROFILE
    pTimerBuffer = calloc((PROF_BUF_SIZE * sizeof(tPPu64)),sizeof(char));
#endif

    memset(ReqMemTab,0,sizeof(tPPQueryMemRecords)*PP_H265DEC_MAX_MEMTAB);

    nCurrThreadID = thread_no ;

    nRetVal = sPP_H265_MultiThreadInit(&nMultiThreadParam ,nCurrThreadID,
        h265_CreateParams.nNumThreads);

    if(nRetVal != SC_PP_SUCCESS)
    {
        printf("Invalid Multi thread init params\n");
        fflush(stdout);
        nInitDone = EC_PP_FAILURE;
        goto EXIT_H265DEC;
    }
    if (nMultiThreadParam.nTaskID == kH265_TASK_MASTER)
    {
        if( BUFFMGR_Init(H265_DPB_SIZE_PROFILE_3_0) ==  EC_PP_FAILURE)
        {
            printf("\nError : Buffer allocation for DPB failed");
            fflush(stdout);
        }

        if((h265_CreateParams.nMetadataType & PP_H265_METADATA_SEI_DATA)
                                                  == PP_H265_METADATA_SEI_DATA)
        {
          H265_dec_InBuff.buf[3]  = malloc(sizeof(tH265_SeiMesseges)+ 4);
        }
        if((h265_CreateParams.nMetadataType & PP_H265_METADATA_VUI_DATA)
                                                  == PP_H265_METADATA_VUI_DATA)
        {
          H265_dec_InBuff.buf[4]  = malloc(sizeof(tH265_VUIParameters)+ 4);
        }
        /* TODO: Ideally number of mem tab entires should be queried
        * before passing the memory table */
        /* Query the decoder for memory requirements*/
        nRetVal = gPP_H265_QueryMemoryRequirements( &ReqMemTab[0],
            &h265_CreateParams, &nNumMemTabEntries);

        if(nRetVal != SC_PP_SUCCESS)
        {
            printf("Error in Finding the memory requirements.\n");
            fflush(stdout);
            nAllocDone = EC_PP_FAILURE;
            goto EXIT_H265DEC;
        }

        /*Allocate the memory*/
        nRetVal = MEMMGR_AllocMemoryRequirements(&ReqMemTab[0],
            nNumMemTabEntries);
        if(nRetVal != SC_PP_SUCCESS)
        {
            printf("Error in Allocating the memory requirements.\n");
            fflush(stdout);
            nAllocDone = EC_PP_FAILURE;
            nSlaveExit = 1;
            goto EXIT_H265DEC;
        }
        /*Pass the memory and Initialize the decoder*/
        nRetVal = gPP_H265_InitializeDecoder  ( &h265dec,
            &ReqMemTab[0],&h265_CreateParams);

        if(nRetVal != SC_PP_SUCCESS)
        {
            printf("Invalid create time params\n");
            fflush(stdout);
            nInitDone = EC_PP_FAILURE;
            nSlaveExit = 1;
            goto EXIT_H265DEC;
        }
        nAllocDone = nRetVal;
        nInitDone  = nRetVal;

        /* Initialise pLum pointers in output base frames to NULL.
        * When the decoder initialises these pointers with valid values we know
        * that its time to dump/display them
        */
        nIndex = 0;
        while( nIndex < (PP_H265VDEC_MAX_REF_FRAMES+1) )
        {
            pOutBuf[nIndex].pLum = NULL;
            nIndex++;
        }

        /*  Call decode bit stream */
        nByteRead = 0;
        nBytesDec = 0;
        nRetV = h265dec->vSet( h265dec, PP_SET_DATASYNC,
            0, &h265_DynamicParams);
        if(nRetV == EC_PP_FAILURE)
        {
            nTerminateStatus = 1;
            goto EXIT_H265DEC;
        }
#ifdef LOW_DELAY_INTERFACE_SLICEMODE
        nNoBytes   = ( Bytes[inputSync_Counter]);
#else
        nNoBytes   = ( H265_PACKET_SIZE - nByteLeft );
#endif
        nFileRead  = ( tPPu32 )fread( Input_buff.nBitStream + nByteLeft, 1 ,
            nNoBytes,fpInput);

        Input_buff.nBufLength = nFileRead + nByteLeft ;
        nBufFilled = Input_buff.nBufLength;
        nBytesDec +=nFileRead;
#ifndef _WIN32
        gPP_PPL_AtomicAdd8((tPPu8 *)&nInitCompleted,1);
#endif
        nStatus = 1;
        printf("\nDecoding ..\n");
        fflush(stdout);
    }
    else
    {
#ifndef _WIN32
         while(!nInitCompleted)
        {
            if(nStatus)
                break;
            if(nSlaveExit)
                goto EXIT_H265DEC;
        }
#endif
    }

#ifdef ARM_PROFILE
    process_time =  0;
    avg_process_time = 0;
    peak_process_time = 0;
    total_time = 0;
#endif
    nFrameDecodeStart = 0 ;

    while(Input_buff.nBufLength > 4)
    {
        if (nMultiThreadParam.nTaskID == kH265_TASK_MASTER)
        {
            nRetV = h265dec->vSet( h265dec, PP_SET_DECODE_HDR,
                h265_DynamicParams.nDecodeHeader, &h265_DynamicParams);
            if(nRetV == EC_PP_FAILURE)
            {
                nTerminateStatus = 1;
                goto EXIT_H265DEC;
            }

            if(h265_DynamicParams.nDecodeHeader == PP_H265_DECODE_ACCESSUNIT)
            {
                H265_dec_BuffEle =  BUFFMGR_GetFreeBuffer();

                /* Assign the buffer allotted by Buffer manager to I/p frame*/
                if(H265_dec_BuffEle)
                {
                    (H265_dec_InBuff.buf[0])= (H265_dec_BuffEle->buf[0]);
                    (H265_dec_InBuff.buf[1])= (H265_dec_BuffEle->buf[1]);
                    (H265_dec_InBuff.buf[2])= (H265_dec_BuffEle->buf[2]);
                    (H265_dec_InBuff.bufId) = (H265_dec_BuffEle->bufId);
                }
                else
                {
                    printf("No Free Buffers available\n");
                    fflush(stdout);
                    nTerminateStatus = 1;
                    goto EXIT_H265DEC;
                }
            }
        }


#ifndef _WIN32
        gPP_PPL_AtomicAdd8((tPPu8 *)&nFrameSync,1);

        while((nFrameSync % h265_CreateParams.nNumThreads) != 0)
        {
            if(nTerminateStatus == 1)
                goto EXIT_H265DEC;
        };

        if (nMultiThreadParam.nTaskID == kH265_TASK_MASTER)
        {
#ifdef ARM_PROFILE
            gettimeofday(&tProcessSt , NULL) ;
#endif
        }
        if(((h265_DynamicParams.nDecodeHeader == PP_H265_PARSE_HEADER)
            && (nMultiThreadParam.nTaskID == kH265_TASK_MASTER))|| 
            (h265_DynamicParams.nDecodeHeader == PP_H265_DECODE_ACCESSUNIT))
        {
            nRetVal = h265dec->vDecode( h265dec, &(Input_buff),
                &(H265_dec_InBuff), &(H265_dec_OutBuff),
                (tPPYUVPlanarDisplayFrame *)(&pOutBuf[0]),
                &nMultiThreadParam);
        }
        if (nMultiThreadParam.nTaskID == kH265_TASK_MASTER)
        {
#ifdef ARM_PROFILE
            gettimeofday(&tProcessEd , NULL) ;
            process_time = (tProcessEd.tv_sec - tProcessSt.tv_sec)*1000000 +
                (tProcessEd.tv_usec - tProcessSt.tv_usec);
            pTimerBuffer [nDecFrmNo] = process_time ;
            total_time += process_time;
#endif
        }
#else
        nRetVal = h265dec->vDecode( h265dec, &(Input_buff),
            &(H265_dec_InBuff), &(H265_dec_OutBuff),
            (tPPYUVPlanarDisplayFrame *)(&pOutBuf[0]),&nMultiThreadParam);
#endif
        if (nMultiThreadParam.nTaskID == kH265_TASK_MASTER)
        {
#ifdef LOW_DELAY_INTERFACE_SLICEMODE
            inputSync_Counter++;
#endif
        }
        if(nRetVal == EC_PP_FAILURE)
        {
            if(nMultiThreadParam.nTaskID == kH265_TASK_MASTER)
            {
                apVal.nError = 0;
                h265dec->vGet(h265dec, PP_GET_ERRORSTATUS,
                    (void *)(&apVal));
                if(apVal.nError !=0)
                {
                    gH265_Error_Report(fErTraceFile, apVal.nError);
                }
                goto EXIT_H265DEC;
            }
        }


        if (nMultiThreadParam.nTaskID == kH265_TASK_MASTER)
        {
            nFrameSync   = 0;
            apVal.nError = 0;
            h265dec->vGet( h265dec, PP_GET_ERRORSTATUS,
                (void *)(&apVal));
            if(apVal.nError !=0)
            {
                gH265_Error_Report(fErTraceFile, apVal.nError);
            }
            if(nDecFrmNo >= 0)
            {
                tPPi32 nTemp =0;                
                h265dec->vGet(h265dec, PP_GET_BUFSTATUS,
                    (void *)(&nTemp));
                if((nTemp == BUFFMGR_FREE) || (nTemp == BUFFMGR_USED))
                {
                    if((h265_DynamicParams.nDecodeHeader ==
                       PP_H265_DECODE_ACCESSUNIT) &&\
                       (H265_dec_BuffEle != NULL))
                    {
                        H265_dec_BuffEle->bufStatus =
                            (BUFFMGR_BufferStatus)nTemp;
                    }
                }
                h265_DynamicParams.nDecodeHeader = PP_H265_DECODE_ACCESSUNIT;
            }
            /* Buflength contains bytes left */
            nByteLeft = Input_buff.nBufLength;
            nBytesConsumed = (nBufFilled - nByteLeft);
            nByteRead = nBufFilled - nByteLeft;
            /* If the end of NAL unit(Start of the next NAL unit)
            not found exit*/
            if( nRetVal == EC_PP_H265_NO_NALU_END_FOUND )
            {
                printf("\nError : NAL unit's end not found\n");
                fflush(stdout);
                goto  EXIT_H265DEC;
            }

            if ( nRetVal == EC_PP_OUT_OF_MEMORY )
            {
                if ( nByteLeft )
                {
                    for ( nIndex = 0; nIndex < nByteLeft; nIndex++)
                    {
                        Input_buff.nBitStream[nIndex]
                        = Input_buff.nBitStream[nByteRead + nIndex ];
                    }
                }
            }
            if (  ( nRetVal == EC_PP_FAILURE )
                ||( nRetVal ==  EC_PP_INVALID_PARAM ) ||
                (nRetVal == EC_PP_NOT_SUPPORTED) )
            {
                /* Failure*/
                nIndex = 0;
                while( nIndex < (PP_H265VDEC_MAX_REF_FRAMES + 1) )
                {
                    if (pOutBuf[nIndex].pLum)
                    {
                        nTOTAL_FRAME_SIZE = pOutBuf[0].nBaseFrame.nWidth *
                            pOutBuf[0].nBaseFrame.nHeight;

                        WriteOutputFrame(&pOutBuf[nIndex], fpOutput);
                        if(nCompareOutput)
                        {
                            nResult =
                                CompareOutputFrame(&pOutBuf[nIndex], fpRef);
                            if(nResult == -1)
                            {
                                printf("Frame Comparision : FAIL\n\n");
                                fflush(stdout);
                            }
                            else
                            {
                                printf("Frame Comparision : PASS\n\n");
                                fflush(stdout);
                            }
                        }
                        pOutBuf[nIndex].pLum = NULL;
                        printf("Frame number %d dumped\n", 
                             nDispFrameNo++);
                        fflush(stdout);     
                    }
                    else
                    {
                        break;
                    }
                    nIndex++;
                }

                /*Call release buffer*/
                BUFFMGR_ReleaseBuffer( &H265_dec_OutBuff.bufId[0]);
                goto  EXIT_H265DEC;
            }

            if ( nByteLeft > 4 )
            {
                /* Buflength contains bytes left*/
                for ( nIndex = 0; nIndex < nByteLeft; nIndex ++)
                {
                    Input_buff.nBitStream[nIndex]
                    = Input_buff.nBitStream[nByteRead + nIndex];
                }
            }

            /* If complete frame is decoded, write that in the output file*/
            if( nRetVal == SC_PP_SUCCESS || nRetVal == SC_PP_RESOLUTION_CHANGED
                ||  nRetVal == SC_PP_END_OF_PICTURE)
            {
                if ( nDecFrmNo == 0 || nRetVal == SC_PP_RESOLUTION_CHANGED)
                {
                    nRetVal =  h265dec->vGet( h265dec, 
                        PP_GET_PARAMSTATUS, (void *)(&apVal));
                    if ( nRetVal != SC_PP_SUCCESS )
                    {
                        printf("Get params failed\n");
                        fflush(stdout);
                    }

                    nTOTAL_FRAME_SIZE = (apVal.nTotalFrameSize);
                    /* inorder to make multiple of 4 ht for inter prediction
                    module */

                    nLumSize = nTOTAL_FRAME_SIZE;
                    nCbSize  = nTOTAL_FRAME_SIZE >> 2;
                    nCrSize  = nTOTAL_FRAME_SIZE >> 2;

                    h265dec->vSet(h265dec, PP_SET_RES_INV,
                        0, &h265_DynamicParams);
                    if ( BUFFMGR_ReInit( nLumSize,
                        (nCbSize + nCrSize)) == -1)
                    {
                        /* Failure*/
                        printf("Re initialization of Buffer failed\n");
                        fflush(stdout);
                        goto  EXIT_H265DEC;
                    }
                }

                nIndex = 0;
                while( nIndex < (PP_H265VDEC_MAX_REF_FRAMES + 1) )
                {

                    if (pOutBuf[nIndex].pLum)
                    {
                        nTOTAL_FRAME_SIZE = pOutBuf[0].nBaseFrame.nWidth *
                            pOutBuf[0].nBaseFrame.nHeight;

                        WriteOutputFrame(&pOutBuf[nIndex], fpOutput);
                        if(nCompareOutput)
                        {
                            nResult =
                                CompareOutputFrame(&pOutBuf[nIndex], fpRef);
                            if(nResult == -1)
                            {
                                printf("Frame Comparision : FAIL\n\n");
                                fflush(stdout);
                            }
                            else
                            {
                                printf("Frame Comparision : PASS\n\n");
                                fflush(stdout);
                            }
                        }
                        printf("Frame number %d dumped\n",
                            nDispFrameNo);
                        fflush(stdout);

                        nDispFrameNo++;
                        pOutBuf[nIndex].pLum = NULL;
                    }
                    else
                    {
                        break;
                    }
                    nIndex++;
                }
                nDecFrmNo++;
                /*Call release buffer*/
                BUFFMGR_ReleaseBuffer( &H265_dec_OutBuff.bufId[0]);
            }/*end of if( nRetVal == SC_PP_SUCCESS )*/
#ifdef LOW_DELAY_INTERFACE_SLICEMODE
            nNoBytes   = Bytes[inputSync_Counter];
#else
            nNoBytes   = ( H265_PACKET_SIZE - nByteLeft );
#endif
            nFileRead  = ( tPPu32 )fread( Input_buff.nBitStream + nByteLeft, 1 ,
                nNoBytes,fpInput);

            Input_buff.nBufLength = nFileRead + nByteLeft ;

            nBufFilled = Input_buff.nBufLength;
        }
#ifndef _WIN32
        if (nMultiThreadParam.nTaskID != kH265_TASK_MASTER)
        {
            if (( nRetVal == EC_PP_FAILURE ) ||
                (nRetVal ==  EC_PP_INVALID_PARAM ) ||
                (nRetVal == EC_PP_NOT_SUPPORTED) ||
                (nRetVal == EC_PP_H265_NO_NALU_END_FOUND))
            {
                goto EXIT_H265DEC ;
            }
            gPP_PPL_AtomicAdd8((tPPu8 *)&nFrameDecodeStart,1);
            do
            {
                if(nFrameDecodeStart == (tPPi32)
                    ((h265_CreateParams.nNumThreads) -1))
                {
                    gPP_PPL_AtomicAdd8((tPPu8 *)&nFrameDecodeEnd,1);
                    break;
                }
            }while(1);
        }
        if (nMultiThreadParam.nThreadID == kH265_TASK_MASTER)
        {
            do
            {
                if(nFrameDecodeEnd ==
                 (tPPi32) ((h265_CreateParams.nNumThreads)  -1))
                {
                    nFrameDecodeStart = 0;
                    nFrameDecodeEnd = 0;
                    break;
                }
            }
            while(1);
        }
        else
        {
            while(nFrameDecodeStart);
        }
#endif
        if(h265_ConfigParams.nNumFramesToDecode <= nDispFrameNo)
        {
            goto EXIT_H265DEC;
        }

    }

    if (nMultiThreadParam.nTaskID == kH265_TASK_MASTER)
    {
        if (Input_buff.nBufLength < 5)
        {
            /* after Decoding Last Frame Dump all the rest frame */
            do
            {
                h265dec->vSet( h265dec , PP_SET_DPB_FLUSH , 
                    PP_TRUE, &h265_DynamicParams);

                nRetVal = h265dec->vDecode( h265dec,
                    &(Input_buff), &(H265_dec_InBuff), &(H265_dec_OutBuff),
                    (tPPYUVPlanarDisplayFrame *)(&pOutBuf[0]), 
                    &nMultiThreadParam);

                if (nRetVal == SC_PP_SUCCESS)
                {
                    nIndex = 0;
                    while( nIndex < (PP_H265VDEC_MAX_REF_FRAMES + 1) )
                    {
                        if (pOutBuf[nIndex].pLum)
                        {
                            nTOTAL_FRAME_SIZE = pOutBuf[0].nBaseFrame.nWidth *
                                pOutBuf[0].nBaseFrame.nHeight;

                            WriteOutputFrame(&pOutBuf[nIndex], fpOutput);

                            if(nCompareOutput)
                            {
                                nResult =
                                    CompareOutputFrame(&pOutBuf[nIndex], fpRef);
                                if(nResult == -1)
                                {
                                    printf("Frame Comparision : FAIL\n\n");
                                    fflush(stdout);
                                }
                                else
                                {
                                    printf("Frame Comparision : PASS\n\n");
                                    fflush(stdout);
                                }
                            }

                            pOutBuf[nIndex].pLum = NULL;
                            printf("Frame number %d dumped\n",
                                nDispFrameNo++);
                            fflush(stdout);    
                        }
                        else
                        {
                            break;
                        }
                        nIndex++;
                    }
                }
                else
                {
                    printf("\nStream End\n");
                    fflush(stdout);
                }
            }while (nRetVal == SC_PP_SUCCESS);
        }
    }

EXIT_H265DEC:

    if (nMultiThreadParam.nThreadID == kH265_TASK_MASTER)
    {
#ifdef ARM_PROFILE
        avg_process_time = ( total_time / nDecFrmNo);
        frame_rate = (nDecFrmNo * (1000000))/(float)total_time;
        nFrmNo = nDecFrmNo;
        peak_process_time = PeakDecodeTime(pTimerBuffer , nDecFrmNo);
        gGetProfileNums();
#endif
        if((nAllocDone == EC_PP_FAILURE) &&
            (nInitDone == SC_PP_SUCCESS))
            /*Deallocate the memory*/
            MEMMGR_DeAllocMemory(&ReqMemTab[0]);

        /* de-initialise buffer management unit */
        BUFFMGR_DeInit();
    }
    /* Delete threads*/
#ifndef _WIN32
    if (nMultiThreadParam.nThreadID != kH265_TASK_MASTER)
    {
        pthread_exit(&nMultiThreadParam.nThreadID);       
    }
#endif
    return 0;
}

/**
******************************************************************************
* @fn PeakDecodeTime(tPPi32 argc, tPPi8 *argv[])
*
* @brief Function to calculate peak decode time
*
* @param pTimerBuffer [IN] Decode time buffer pointer
* @param nNumFrame    [IN] Number of frames decoded
*
* @return Peak time in micro seconds
******************************************************************************
*/
#ifdef ARM_PROFILE
/*Moving average for peak decode time call calculation*/
tPPu64  PeakDecodeTime( tPPu64  *pTimerBuffer,
                       tPPu32  nNumFrame)
{
    tPPu64  nPeak;
    tPPu64  nPeakTime;
    tPPu32 i,j,nCount;
    nPeakTime = 0;
    for(i = 0 ; i < nNumFrame ;i++)
    {
        nPeak = 0;
        nCount = 30;
        for(j = i; j < (30+i); j++)
        {
            nPeak += pTimerBuffer[j];
            if(pTimerBuffer[j] == 0)
            {
                nCount --;
            }
        }
        nPeak = nPeak/nCount;
        nPeakTime = nPeak > nPeakTime ? nPeak : nPeakTime;
    }
    return nPeakTime;
}
#endif
/*
******************************************************************************
* @fn gPP_H265_MTInit(tPPi32 argc, tPPi8 *argv[])
*
* @brief Function to initialize multi thread parameters
*
* @param nMultiThreadParam [IN] Structure
*
* @return Number of bytes written to the file
******************************************************************************
*/
static tPPi32 sPP_H265_MultiThreadInit(tPPMultiThreadParams *nMultiThreadParam,
                                       tPPu32 nThreadID, tPPu32 nNumThreads)
{
    /*Task ID*/
    if(nThreadID == 0)
        nMultiThreadParam->nTaskID = kH265_TASK_MASTER ;
    else
        nMultiThreadParam->nTaskID = kH265_TASK_SLAVE ;
    /*Number of threads*/
    nMultiThreadParam->nNumThreads = nNumThreads ;
    /*Thread ID */
    nMultiThreadParam->nThreadID = nThreadID ;

    return SC_PP_SUCCESS ;

}
/**
********************************************************************************
*  @fn     gH265_getDataSync( tPP_H265_DataSyncDesc *pDataSyncParams)
*
*  @brief  The function is an Application function to fill datasync parameters
*
*  @param[IN/OUT] pDataSyncParams  : datasync descriptor
*
*  @return  None
********************************************************************************
*/
void gH265_getDataSync(tPP_H265_DataSyncDesc *pDataSyncParams)
{
#ifdef LOW_DELAY_INTERFACE_SLICEMODE
    tPPu32 nIndex = 0;
    nByteLeft_Sync = Input_buff.nBufLength;
    inputSync_Counter++;

    if ( nByteLeft_Sync )
    {
        /* Buflength contains bytes left*/
        for ( nIndex = 0; nIndex < nByteLeft_Sync; nIndex ++)
        {
            Input_buff.nBitStream[nIndex]
            = Input_buff.nBitStream[nByteRead_Sync + nIndex];
        }
    }
    nNoBytes_Sync   = Bytes[inputSync_Counter];
    nFileRead_Sync  = ( tPPu32 )fread( Input_buff.nBitStream + nByteLeft_Sync,
         1 ,nNoBytes_Sync, fpInput);
    pDataSyncParams->nSize = nFileRead_Sync + nByteLeft_Sync;
    nBufFilled_Sync = pDataSyncParams->nSize;
#endif
}
/**
********************************************************************************
*  @fn     gH265_putDataSync(tPP_H265_DataSyncDesc *pDataSyncParams)
*
*  @brief  The function is an Application function to fill datasync parameters
*
*  @param[IN/OUT] pDataSyncParams  : datasync descriptor
*
*  @return  None
********************************************************************************
*/
void gH265_putDataSync(tPP_H265_DataSyncDesc *pDataSyncParams)
{
    tPPu32 nNumRows = pDataSyncParams->nNumBlocks;
    /*Data sync code for validation---->Start(Data sync)*/
    tPPi32 nCurrHeight;
    tPPu8 *pLum;
    tPPu8 *pCb;
    tPPu8 *pCr;
    tPPi32 nPicHeight,nPicWidth ,nSize ,nTotBytes =0;

    tPPYUVPlanarDisplayFrame *pOutFrame_DS = &pOutBuf[0];
    /* init */
    nPicHeight = pOutFrame_DS->nBaseFrame.nHeight;
    nPicWidth  = pOutFrame_DS->nBaseFrame.nWidth;
    nCurrHeight =  pDataSyncParams->nNumBlocks << 4;

    nRowNum += nNumRows;
    if(nNumRows)
    {

        pLum = ybuf +(nPicWidth * gCurrHeight);
        pCb =  cbbuf +((nPicWidth * gCurrHeight) >> 2);
        pCr = crbuf + ((nPicWidth * gCurrHeight) >> 2);
        memcpy(pLum,(pOutFrame_DS->pLum + (nPicWidth * gCurrHeight)),
            nCurrHeight*nPicWidth);
        memcpy(pCb,(pOutFrame_DS->pCb + ((nPicWidth * gCurrHeight) >> 2)),
            ((nCurrHeight*nPicWidth)>>2));
        memcpy(pCr,(pOutFrame_DS->pCr + ((nPicWidth * gCurrHeight) >> 2)),
            ((nCurrHeight*nPicWidth)>>2));
        gCurrHeight = gCurrHeight + nCurrHeight;
        if(gCurrHeight >= nPicHeight)
        {
            gCurrHeight = 0;
            nSize = pOutFrame_DS->nBaseFrame.nWidth *
                pOutFrame_DS->nBaseFrame.nHeight;
            nTotBytes = fwrite(ybuf , 1, nSize,      fpDataSync);
            nTotBytes = fwrite(cbbuf, 1, nSize >> 2, fpDataSync);
            nTotBytes = fwrite(crbuf, 1, nSize >> 2, fpDataSync);
        }
        /*  Increment the row counter by number of rows output in
        *  this datasync call
        */
    }
    nDataSyncIntrCntr++;
}

/**
******************************************************************************
* @fn static tPPu32 WriteOutputFrame(tPPYUVPlanarDisplayFrame *pOutFrame,
*                                    FILE *fpOutput)
*
* @brief Function to write the output reconcstructed YUV frame to file
*
* @param pOutFrame [IN] Reconstructed YUV frame pointer
*
* @param fpOutput  [IN] File Pointer
*
* @return Number of bytes written to the file
******************************************************************************
*/
static tPPu32 WriteOutputFrame(tPPYUVPlanarDisplayFrame *pOutFrame,
                               FILE  *fpOutput)
{
#ifdef FILE_DUMP
    tPPu32 nTotBytes = 0;
    tPPi32 i, nSize;
    tPPu8 *pSrc;

    if(pOutFrame->nBaseFrame.nIsPadded)
    {
        pSrc = pOutFrame->pLum;
        nSize = pOutFrame->nBaseFrame.nWidth;
        for(i = 0; i < pOutFrame->nBaseFrame.nHeight; i++)
        {
            nTotBytes += fwrite(pSrc, 1, nSize, fpOutput);
            pSrc += pOutFrame->nBaseFrame.nExWidth;
        }

        pSrc = pOutFrame->pCb;
        nSize = pOutFrame->nBaseFrame.nWidth >> 1;
        for(i = 0; i < pOutFrame->nBaseFrame.nHeight >> 1; i++)
        {
            nTotBytes += fwrite(pSrc, 1, nSize, fpOutput);
            pSrc += (pOutFrame->nBaseFrame.nExWidth >> 1);
        }
        pSrc = pOutFrame->pCr;
        nSize = pOutFrame->nBaseFrame.nWidth >> 1;
        for(i = 0; i < pOutFrame->nBaseFrame.nHeight >> 1; i++)
        {
            nTotBytes += fwrite(pSrc, 1, nSize, fpOutput);
            pSrc += (pOutFrame->nBaseFrame.nExWidth >> 1);
        }
    }
    else
    {
        nSize = pOutFrame->nBaseFrame.nWidth * pOutFrame->nBaseFrame.nHeight;
        {
            nTotBytes = fwrite(pOutFrame->pLum, 1, nSize, fpOutput);
            nTotBytes = fwrite(pOutFrame->pCb, 1, nSize >> 2, fpOutput);
            nTotBytes = fwrite(pOutFrame->pCr, 1, nSize >> 2, fpOutput);
        }
    }

    return nTotBytes;
#else
    return 0;
#endif
}
/**
******************************************************************************
* @fn static tPPi32 CompareOutputFrame(tPPYUVPlanarDisplayFrame *pOutFrame,
*                                      FILE *fpRef)
*
* @brief Function to Compare the output YUV frame to reference YUV file
*
* @param pOutFrame [IN] Reconstructed YUV frame pointer
*
* @param fpRef     [IN] File Pointer
*
* @return
******************************************************************************
*/
static tPPi32 CompareOutputFrame(tPPYUVPlanarDisplayFrame *pOutFrame,
                               FILE  *fpRef)
{
    tPPi32 i, nSize;
    tPPu8 *pSrc;
    tPPu8 *pRefBuf;
    tPPu32 nFrameSizeLuma ,nFrameSizeChroma;

    nFrameSizeLuma = pOutFrame->nBaseFrame.nWidth *
        pOutFrame->nBaseFrame.nHeight;
    nFrameSizeChroma = nFrameSizeLuma >> 2;

    pSrc = pOutFrame->pLum;
    pRefBuf = nRefBuf;
    nSize = pOutFrame->nBaseFrame.nWidth;
    for(i = 0; i < pOutFrame->nBaseFrame.nHeight; i++)
    {
        fread(pRefBuf,  1, nSize, fpRef);
        pRefBuf += nSize;
    }
    /* Compare Luma component */
    if (memcmp(pSrc,nRefBuf,nFrameSizeLuma))
    {
        printf("\nFailed in luma comparision\n");
        fflush(stdout);
        return EC_PP_FAILURE;
    }

    pSrc = pOutFrame->pCb;
    pRefBuf = nRefBuf;
    nSize = pOutFrame->nBaseFrame.nWidth >> 1;
    for(i = 0; i < pOutFrame->nBaseFrame.nHeight >> 1; i++)
    {
        fread(pRefBuf,  1, nSize, fpRef);
        pRefBuf += nSize;

    }
    /* Compare Luma component */
    if (memcmp(pSrc,nRefBuf,nFrameSizeChroma))
    {
        printf("\nFailed in Chroma comparision\n");
        fflush(stdout);
        return EC_PP_FAILURE;
    }

    pSrc = pOutFrame->pCr;
    pRefBuf = nRefBuf;
    nSize = pOutFrame->nBaseFrame.nWidth >> 1;
    for(i = 0; i < pOutFrame->nBaseFrame.nHeight >> 1; i++)
    {
        fread(pRefBuf,  1, nSize, fpRef);
        pRefBuf += nSize;
    }
    /* Compare Luma component */
    if (memcmp(pSrc,nRefBuf,nFrameSizeChroma))
    {
        printf("\nFailed in Chroma comparision\n");
        fflush(stdout);
        return EC_PP_FAILURE;
    }
    return 0;
}
#ifdef ARM_PROFILE
/**
******************************************************************************
* @fn static void gGetProfileNums()
*
* @brief Function to print profile values
*
* @return
******************************************************************************
*/
static void gGetProfileNums()
{

    printf("***************Profile statistics***************\n");
    fflush(stdout);
    printf(" Frames decoded                  : %d \n",nFrmNo);
    fflush(stdout);
    printf(" Total decode time               : %lld.%lld ms\n",
        total_time/1000,total_time%1000);
    fflush(stdout);
    printf(" Peak decode time per frame      : %lld.%lld ms\n",
        peak_process_time/1000,peak_process_time%1000);
    fflush(stdout);
    printf(" Average decode time per frame   : %lld.%lld ms\n",
        avg_process_time/1000,avg_process_time%1000);
    fflush(stdout);
    printf(" Frames per second               : %0.2f fps\n",frame_rate);
    fflush(stdout);
    printf("************************************************\n");
    fflush(stdout);

}
#endif
