/** ===========================================================================
* @file h265vdec_pp_buffer_mnge.c
*
* @brief This file contains implementation of all the buffer manager functions
*        for PathPartner's H265 decoder
*
* ============================================================================
*
* Copyright � PathPartner Technology, 2012-2015
*
* This material,including documentation and  any related computer programs,is
* protected by copyright controlled by PathPartner Technology. All rights are
* reserved.
*
* ============================================================================
*
* <b> REVISION HISTORY </b>
* @n  ================  @n
* @version 0.1 : 30-Jun-2012 :  Sunil K : Initial Code
* @version 0.2 : 20-jul-2014 :  Karthik SM :Modiofied alloc and dealloc
* $RevLog$
*
*=============================================================================
*/
/** ===========================================================================
* @file buffermanager.c
*
* @path $(PROJDIR)\src
*
****************************************************************/

/*******************************************************************************
*   INCLUDE FILES
*******************************************************************************/

#include "h265vdec_pp_buffer_manager.h"

/*******************************************************************************
*   MACROS 
*******************************************************************************/

#define OUTPUT_BUFFER_MEM_SIZE (98*1024*1024)

/*******************************************************************************
*   GLOBAL VARIABLES 
*******************************************************************************/

tPPu8   outputBufferMemory[OUTPUT_BUFFER_MEM_SIZE];
tPPu8  *pOutputBufferMemory    = outputBufferMemory;
tPPu32 outputBufferMemorySize  = OUTPUT_BUFFER_MEM_SIZE;

/****************************************************************
*  PUBLIC DECLARATIONS Defined here, used elsewhere
****************************************************************/
/*--------data declarations -----------------------------------*/
BUFFMGR_buffEle buffArray[MAX_BUFF_ELEMENTS];
tPPu8 *lumaGlobalBufferHandle = NULL;
tPPu8 *chromaCbGlobalBufferHandle = NULL;
tPPu8 *chromaCrGlobalBufferHandle = NULL;
tPPu32 lumaGlobalBufferSize;
tPPu32 chromaGlobalBufferSize;
tPPu32 chromaCbGlobalBufferSize;
tPPu32 chromaCrGlobalBufferSize;
/*--------function prototypes ---------------------------------*/
/****************************************************************
*  PRIVATE DECLARATIONS Defined here, used only here
****************************************************************/
/*--------data declarations -----------------------------------*/
/*--------function prototypes ---------------------------------*/
/*--------macros ----------------------------------------------*/
/*--------function definitions---------------------------------*/



/*****************************************************************************/
/**
 * @fn BUFFMGR_Init(tPPu32 totBufSize)
 *
 * @brief  Implementation of buffer manager initialization module
 *
 *        The BUFFMGR_Init function is called by the test application to
 *        initialise the global buffer element array to default and to allocate
 *        required number of memory data for reference and output buffers.
 *        The maximum required dpb size is defined by the supported profile &
 *        level.
 *
 * @param  totBufSize
 *        Total buffer size to be allocated
 *
 * @return Success(0)/failure(-1) in allocating and initialising
 *
 */
/*****************************************************************************/
tPPi32 BUFFMGR_Init(tPPu32 totBufSize)
{
    tPPu32 tmpCnt;
    /* total buffer size allocatable is divided into three parts one part goes
     * to luma buffers and the other two parts go to luma buffers */
    chromaGlobalBufferSize = (totBufSize/3);
    chromaCbGlobalBufferSize = chromaGlobalBufferSize >> 1;
    chromaCrGlobalBufferSize = chromaGlobalBufferSize >> 1;

    lumaGlobalBufferSize   = (chromaGlobalBufferSize*2);

    if(outputBufferMemorySize < lumaGlobalBufferSize)
    {
        return EC_PP_FAILURE;
    }
    /*------------------------------------------------------------------------*/
    /* Allocate the global buffers                                            */
    /*------------------------------------------------------------------------*/
    lumaGlobalBufferHandle = pOutputBufferMemory;
    pOutputBufferMemory += lumaGlobalBufferSize;
    /*------------------------------------------------------------------------*/
    /* Upadate the memory after allocating to luma buffer                     */
    /*------------------------------------------------------------------------*/
    outputBufferMemorySize -= lumaGlobalBufferSize;

/*------------------------------------------------------------------------*/
/* Check whether the memory requirement is reaching beyond the available  */
/*------------------------------------------------------------------------*/
    if(outputBufferMemorySize < chromaCbGlobalBufferSize)
    {
        lumaGlobalBufferHandle = 0;
        pOutputBufferMemory = outputBufferMemory;
        outputBufferMemorySize = OUTPUT_BUFFER_MEM_SIZE;
        return EC_PP_FAILURE;
    }
    /*------------------------------------------------------------------------*/
    /* Allocate the global buffers                                            */
    /*------------------------------------------------------------------------*/
    chromaCbGlobalBufferHandle = pOutputBufferMemory;
    pOutputBufferMemory += chromaCbGlobalBufferSize;
    /*------------------------------------------------------------------------*/
    /* Upadate the memory after allocating to luma buffer                     */
    /*------------------------------------------------------------------------*/
    outputBufferMemorySize -= chromaCbGlobalBufferSize;

    /*------------------------------------------------------------------------*/
    /* Check whether the memory requirement is reaching beyond the available  */
    /*------------------------------------------------------------------------*/
    if(outputBufferMemorySize < chromaCrGlobalBufferSize)
    {
        lumaGlobalBufferHandle = 0;
        chromaCbGlobalBufferHandle = 0;
        pOutputBufferMemory = outputBufferMemory;
        outputBufferMemorySize = OUTPUT_BUFFER_MEM_SIZE;
        return EC_PP_FAILURE;
    }
    /*------------------------------------------------------------------------*/
    /* Allocate the global buffers                                            */
    /*------------------------------------------------------------------------*/
    chromaCrGlobalBufferHandle = pOutputBufferMemory;
    pOutputBufferMemory += chromaCrGlobalBufferSize;
    /*------------------------------------------------------------------------*/
    /* Upadate the memory after allocating to luma buffer                     */
    /*------------------------------------------------------------------------*/
    outputBufferMemorySize -= chromaCrGlobalBufferSize;


    /* Initialise the elements in the global buffer array */
    for(tmpCnt = 0; tmpCnt < MAX_BUFF_ELEMENTS; tmpCnt++)
    {
        buffArray[tmpCnt].bufId = tmpCnt+1;
        buffArray[tmpCnt].bufStatus = BUFFMGR_FREE;
        buffArray[tmpCnt].bufSize[2] = 0;
        buffArray[tmpCnt].bufSize[1] = 0;
        buffArray[tmpCnt].bufSize[0] = 0;
        buffArray[tmpCnt].buf[2] = NULL;
        buffArray[tmpCnt].buf[1] = NULL;
        buffArray[tmpCnt].buf[0] = NULL;
    }

    /* Initialise the entire buffer space to first frame and
     * re-modify buffer sizes as per the picture frame sizes
     * after first frame decode */
     buffArray[0].buf[2] = chromaCrGlobalBufferHandle;
     buffArray[0].bufSize[2] = chromaCrGlobalBufferSize;
     buffArray[0].buf[1] = chromaCbGlobalBufferHandle;
     buffArray[0].bufSize[1] = chromaCbGlobalBufferSize;
     buffArray[0].buf[0] = lumaGlobalBufferHandle;
     buffArray[0].bufSize[0] = lumaGlobalBufferSize;
     return SC_PP_SUCCESS;
}


/*****************************************************************************/
/**
 * @fn tPPi32 BUFFMGR_ReInit(tPPu32 lumaOneFrameBufSize,
 *                    tPPu32 chromaOneFrameBufSize)
 * @brief  Implementation of buffer manager
 *       re-initialization module
 *
 *        The BUFFMGR_ReInit function allocates global luma and chroma buffers
 *       and allocates entire space to first element. This element will be used
 *       in first frame decode. After the picture's height and width and its
 *       luma and chroma buffer requirements are obtained the global luma and
 *       chroma buffers are re-initialised to other elements in teh buffer arary
 *
 * @param  lumaOneFrameBufSize
 *        Buffer size for one luma frame in bytes
 *
 * @param  chromaOneFrameBufSize
 *        Buffer size for one chroma frame in bytes
 *
 * @return Success(0)/failure(-1) in allocating and initialising
 *
 */
/*****************************************************************************/
tPPi32 BUFFMGR_ReInit(tPPu32 lumaOneFrameBufSize,
                      tPPu32 chromaOneFrameBufSize)
{
    tPPu32 tmpLum, tmpChrmCb, tmpChrmCr,tmpCnt;
    tPPu8 *tmpLumaBuf, *tmpChrmCbBuf,*tmpChrmCrBuf;
    tPPu32 chromaCbOneFrameBufSize,chromaCrOneFrameBufSize;

    /* check if the requested sizes exceed allocated buffer sizes */
    if((lumaOneFrameBufSize > lumaGlobalBufferSize) ||
        (chromaOneFrameBufSize > chromaGlobalBufferSize))
    {
        return EC_PP_FAILURE;
    }

    chromaCbOneFrameBufSize = (chromaOneFrameBufSize >> 1);
    chromaCrOneFrameBufSize =  chromaCbOneFrameBufSize;

    tmpLum = lumaGlobalBufferSize;
    tmpChrmCb = chromaCbGlobalBufferSize;
    tmpChrmCr = chromaCrGlobalBufferSize;
    tmpLumaBuf = lumaGlobalBufferHandle;
    tmpChrmCbBuf = chromaCbGlobalBufferHandle;
    tmpChrmCrBuf = chromaCrGlobalBufferHandle;

    /* now re-allocate buffer sizes for each element based on the
    * per frame buffer requirements */
    for(tmpCnt = 0;
        (tmpCnt < MAX_BUFF_ELEMENTS) &&
        ((tmpLum >= lumaOneFrameBufSize)) &&
        ((tmpChrmCb >= chromaCbOneFrameBufSize)) &&
        ((tmpChrmCr >= chromaCrOneFrameBufSize) );
    tmpCnt++)
    {
        buffArray[tmpCnt].buf[0] = tmpLumaBuf;
        buffArray[tmpCnt].buf[1] = tmpChrmCbBuf;
        buffArray[tmpCnt].buf[2] = tmpChrmCrBuf;

        buffArray[tmpCnt].bufSize[2] = chromaCrOneFrameBufSize;
        buffArray[tmpCnt].bufSize[1] = chromaCbOneFrameBufSize;
        buffArray[tmpCnt].bufSize[0] = lumaOneFrameBufSize;
        /* update the local variables for next iteration */
        tmpLum -= lumaOneFrameBufSize;
        tmpChrmCb -= chromaCbOneFrameBufSize;
        tmpChrmCr -= chromaCrOneFrameBufSize;
        tmpLumaBuf += lumaOneFrameBufSize;
        tmpChrmCbBuf += chromaCbOneFrameBufSize;
        tmpChrmCrBuf += chromaCrOneFrameBufSize;
    }

    /* Initialise rest of the elements to NULL - not really required since
    * they are anyway initialised to these default values in Init function */
    for(; (tmpCnt < MAX_BUFF_ELEMENTS); tmpCnt++)
    {
        buffArray[tmpCnt].buf[0] = NULL;
        buffArray[tmpCnt].buf[1] = NULL;
        buffArray[tmpCnt].buf[2] = NULL;
        buffArray[tmpCnt].bufSize[2] = 0;
        buffArray[tmpCnt].bufSize[1] = 0;
        buffArray[tmpCnt].bufSize[0] = 0;
    }
    return 0;
}


/*****************************************************************************/
/**
* @fn BUFFMGR_buffEleHandle BUFFMGR_GetFreeBuffer()
*
* @brief  Implementation of buffer manager get free buffer module.
*         The BUFFMGR_GetFreeBuffer function searches for a free buffer in the
*         global buffer array and returns the address of that element. Incase
*         if none of the elements are free then it returns NULL
*
*
* @return Valid buffer element address or NULL incase if no buffers are empty
*
*/
/*****************************************************************************/
BUFFMGR_buffEleHandle BUFFMGR_GetFreeBuffer()
{
    tPPu32 tmpCnt;
    /* Since buffers are re-initialised later on its perfectly possible to
    * have buffer elements with NULL as the buffer pointers - as soon as
    * such buffer elements are encountered we need to stop search for
    * free buffers
    */
    for(tmpCnt = 0;
        (tmpCnt < MAX_BUFF_ELEMENTS) && (buffArray[tmpCnt].buf[0] != NULL);
        tmpCnt++)
    {
        /* Check for first empty buffer in the array and return its address */
        if(buffArray[tmpCnt].bufStatus == BUFFMGR_FREE)
        {
            buffArray[tmpCnt].bufStatus = BUFFMGR_USED;
            return (&buffArray[tmpCnt]);
        }
    }
    printf("Run short of frames !!\n");
    fflush(stdout);
    /* Incase if no elements in the array are free then return NULL */
    return NULL;
}


/*****************************************************************************/
/**
* @fn void BUFFMGR_ReleaseBuffer(buffId)
* @brief  Implementation of buffer manager
*       release module
*
*        The BUFFMGR_ReleaseBuffer function takes an array of buffer-ids
*        which are released by the test-app. "0" is not a valid buffer Id
*        hence this function keeps moving until it encounters a buffer Id
*        as zero or it hits the MAX_BUFF_ELEMENTS
*
*
* @return None
*
*/
/*****************************************************************************/
void BUFFMGR_ReleaseBuffer(tPPi32 bufffId[])
{
    tPPu32 tmpCnt, tmpId;
    for(tmpCnt = 0;
        (tmpCnt < MAX_BUFF_ELEMENTS);
        tmpCnt++)
    {
        tmpId = bufffId[tmpCnt];
        /*
        * Check if the buffer Id = 0 condition has reached. zero is not a
        * valid buffer Id hence that value is used to identify the end of
        * buffer array
        */
        if(tmpId == 0)
        {
            break;
        }
        /*
        * convert the buffer-Id to its corresponding index in the global
        * array
        */
        tmpId--;

        if(tmpId >= MAX_BUFF_ELEMENTS) {
            /* Inidcates an invalid buffer Id passed - this buffer Id can be
            * ignored!! alternatively we can break here.
            */
            
            printf("Trying to release a buffer using an invalid bufferId %d \
            ignoring..\n", tmpId+1);
            fflush(stdout);
            continue;
        }
        if(buffArray[tmpId].bufStatus == BUFFMGR_FREE) {
            /* Trying to release an already free bufffer this idicates some
            *  mismanagement in buffer usage following printf will help
            * application developer to identify any such problem in her
            * algorithm
            */
            printf("Trying to release an already free buffer Id %d \n",
                tmpId+1);
            fflush(stdout);
        }
        /* Set the status of the buffer to FREE */
        buffArray[tmpId].bufStatus = BUFFMGR_FREE;
    }

    return;
}

/*****************************************************************************/
/**
* @fn void BUFFMGR_DeInit()
* @brief  Implementation of buffer manager de-initialization module
*         The BUFFMGR_DeInit function releases all memory allocated by buffer
*           manager.
*
* @return None
*
*/
/*****************************************************************************/
void BUFFMGR_DeInit()
{
    if(lumaGlobalBufferHandle)
    {
        lumaGlobalBufferHandle = 0;
    }
    if(chromaCbGlobalBufferHandle)
    {
        chromaCbGlobalBufferHandle = 0;
    }
    if(chromaCrGlobalBufferHandle)
    {
        chromaCrGlobalBufferHandle = 0;
    }
    return;
}
