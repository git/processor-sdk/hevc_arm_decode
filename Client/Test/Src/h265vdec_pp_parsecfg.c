/** ===========================================================================
* @file h265vdec_pp_parsecfg.c
*
* @brief This file parses input config file
* 
* ============================================================================
*
* Copyright � PathPartner Technology, 2012-2015
*
* This material,including documentation and  any related computer programs,is
* protected by copyright controlled by PathPartner Technology. All rights are
* reserved.
*
* ============================================================================
*
* <b> REVISION HISTORY </b>
* @n  ================  @n
* @version 0.1 : 10-Oct-2012 : Praveen GB : Initial Code
* @version 0.2 : 02-July-2014 : Karthik SM : Added config file parameters
* $RevLog$
*
*=============================================================================
*/

/*******************************************************************************
*   INCLUDE FILES
*******************************************************************************/

#include "h265vdec_pp_parsecfg.h"

/*******************************************************************************
*   GLOBAL VARIABLES 
*******************************************************************************/

static char buf[20000];
/*Global structure to store config params*/
extern tPP_H265_ConfigParams h265_ConfigParams;

#define gConfig h265_ConfigParams
/*----------------------------------------------------------------------------*/
/* Array of elements of type sTokenMapping for parsing and holding the tokens */
/* from the input configuration file.                                         */
/*----------------------------------------------------------------------------*/
static tH265CfgParseTokenMap sTokenMap[MAX_ITEMS_TO_PARSE] =
{
  /*--------------------------------------------------------------------------*/
  /* Input file name along with path and type of element is string            */
  /*--------------------------------------------------------------------------*/
  {(tPPi8 *)"NumThreads",&gConfig.nNumThreads,sizeof(gConfig.nNumThreads)},
  /*--------------------------------------------------------------------------*/
  /* Input file name along with path and type of element is string            */
  /*--------------------------------------------------------------------------*/
  {(tPPi8 *)"InputFile",&gConfig.nInputFile,0},
  /*--------------------------------------------------------------------------*/
  /* Input file name along with path and type of element is string            */
  /*--------------------------------------------------------------------------*/
  {(tPPi8 *)"OutputFile",&gConfig.nOutputFile,0},
  /*--------------------------------------------------------------------------*/
  /* Reference file to bitmatch with encoded bit stream                       */
  /*--------------------------------------------------------------------------*/
  {(tPPi8 *)"ReferenceFile",&gConfig.nRefFile,0},

  /*--------------------------------------------------------------------------*/
  /* Width of input bit stream                                                */
  /*--------------------------------------------------------------------------*/
  {(tPPi8 *)"ImageWidth",&gConfig.nMaxWidth,sizeof(gConfig.nMaxWidth)},
  /*--------------------------------------------------------------------------*/
  /* Height of the bit stream                                                 */
  /*--------------------------------------------------------------------------*/
  {(tPPi8 *)"ImageHeight",&gConfig.nMaxHeight,sizeof(gConfig.nMaxHeight)},
  /*--------------------------------------------------------------------------*/
  /* Total number of frames to encode                                         */
  /*--------------------------------------------------------------------------*/
  {(tPPi8 *)"FramesToDecode",&gConfig.nNumFramesToDecode,
                                           sizeof(gConfig.nNumFramesToDecode)},
  /*--------------------------------------------------------------------------*/
  {(tPPi8 *)"InputLowDelayMode",&gConfig.nInputLowDelayMode,
                                       sizeof(gConfig.nInputLowDelayMode)},

  {(tPPi8 *)"OutputLowDelayMode",&gConfig.nOutputLowDelayMode,
                                       sizeof(gConfig.nOutputLowDelayMode)},

  {(tPPi8 *)"NumCTURows",&gConfig.nNumCTURows,
                                           sizeof(gConfig.nNumCTURows)},

  {(tPPi8 *)"MetadataType",&gConfig.nMetadataType,
                                           sizeof(gConfig.nMetadataType)},

  {(tPPi8 *)"DecodeHeader",&gConfig.nDecodeHeader,
                                           sizeof(gConfig.nDecodeHeader)},
  /*--------------------------------------------------------------------------*/
  /* ALWAYS the last element in the map                                       */
  /*--------------------------------------------------------------------------*/
  {(tPPi8 *)"\0",NULL,0}
};
/**
********************************************************************************
 *  @func   ParameterNameToMapIndex
 *
 *  @desc   Returns the index number from sTokenMap[] for a given parameter
 *          name
 *
 *  @param  s : Name string.
 *
 *  @param  sTokenMap : Token map strucutre for variables specified in config
 *                      file.
 *
 *  @return Index number if the string is a valid parameter name,
 *          EC_PP_FAILURE for error
 *******************************************************************************
 */
static tPPi32 ParameterNameToMapIndex(char *s,tH265CfgParseTokenMap *sTokenMap)
{
    tPPi32 i = 0;
    /*------------------------------------------------------------------------*/
    /* Loop for all the variables in the token map for variables specified in */
    /* Decoder configuration file                                             */
    /*------------------------------------------------------------------------*/
    while(sTokenMap[i].pTokenName != NULL)
    {
        if(0 == strcmp((const char *)sTokenMap[i].pTokenName, s))
        {
            return i;
        }
        else
        {
            i++;
        }
    }
    /*------------------------------------------------------------------------*/
    /* Return "EC_PP_FAILURE" in case no record is found                      */
    /*------------------------------------------------------------------------*/
    return EC_PP_FAILURE;
} /* ParameterNameToMapIndex() */
/**
********************************************************************************
*  @fn     ParseContent
*
*  @desc   Parses the character array buf and writes global variable input,
*          which is defined in configfile.h.  This hack will continue to be
*          necessary to facilitate the addition of new parameters through the
*          sTokenMap[] mechanism-Need compiler-generated addresses in
*          sTokenMap
*
*  @param  buf : buffer to be parsed.
*
*  @param  bufsize : size of buffer.
*
*  @param  sTokenMap : Pointer to token map strucutre
*
*  @param  params : Pointer to Decoder instarnce creation params
*
*  @return status ( PASS/ FAIL)
*******************************************************************************
*/
static tPPi32 ParseContent (char *buf, tPPi32 bufsize)
{
    char  *items[MAX_ITEMS_TO_PARSE];
    tPPi32 MapIdx;
    tPPi32 item     = 0;
    tPPi32 InString = 0;
    tPPi32 InItem   = 0;
    char   *p       = buf;
    char  *bufend  = &buf[bufsize];
    tPPi32 IntContent;
    tPPi32 i;
    FILE       *fpErr   = stderr;
    /*------------------------------------------------------------------------*/
    /* Stage one: Generate an argc/argv-type list in items[], without comments*/
    /* and whitespace.                                                        */
    /* This is context insensitive and could be done most easily with lex(1). */
    /*------------------------------------------------------------------------*/
    while (p < bufend)
    {
        switch (*p)
        {
            case 13:
                    p++;
                    break;
            case '#':
                    /*--------------------------------------------------------*/
                    /* Found comment Replace '#' with '\0' in case of comment */
                    /* immediately following integer or string Skip till      */
                    /*  EOL or EOF, whichever comes first                     */
                    /*--------------------------------------------------------*/
                    *p = '\0';
                    while (*p != '\n' && p < bufend)
                    {
                        p++;
                    }
                    InString = 0;
                    InItem = 0;
                    break;
            case '\n':
                    InItem   = 0;
                    InString = 0;
                    *p++='\0';
                    break;
            case ' ':
            case '\t':
                    /*--------------------------------------------------------*/
                    /* Skip whitespace, leave state unchanged                 */
                    /*--------------------------------------------------------*/
                    if (InString)
                    {
                        p++;
                    }
                    else
                    {
                        /*----------------------------------------------------*/
                        /* Terminate non-strings once whitespace is found     */
                        /*----------------------------------------------------*/
                        *p++ = '\0';
                        InItem = 0;
                    }
                    break;

            case '"':
                        /*----------------------------------------------------*/
                        /* Begin/End of String                                */
                        /*----------------------------------------------------*/
                    *p++ = '\0';
                    if (!InString)
                    {
                        items[item++] = p;
                        InItem = ~InItem;
                    }
                    else
                    {
                        InItem = 0;
                    }
                    InString = ~InString; /* Toggle */
                    break;

            default:
                    if (!InItem)
                    {
                        items[item++] = p;
                        InItem = ~InItem;
                    }
                    p++;
        }
    }

    item--;
    /*------------------------------------------------------------------------*/
    /* Parse every item read above and place them in various places           */
    /*------------------------------------------------------------------------*/
    for(i=0 ; i<item ; i += 3)
    {
        MapIdx = ParameterNameToMapIndex(items[i],sTokenMap);
        /*----------------------------------------------------------------*/
        /* All the parameters other than Coreteam mapping can be placed in*/
        /* token map structure                                            */
        /*----------------------------------------------------------------*/
        if (strcmp ("=", items[i+1]))
        {
            fprintf(fpErr,
                "\nfile:'=' expected as the second token in each line.");
            return EC_PP_FAILURE ;
        }
        if(MapIdx == EC_PP_FAILURE)
        {
            return EC_PP_FAILURE;
        }
        if(sTokenMap[MapIdx].nType == 0)
        {
           strcpy((char *)sTokenMap[MapIdx].pPlace, (char *)items[i+2]);
        }
        else if (sTokenMap[MapIdx].nType == 4)
        {
             sscanf ((const char *)items[i+2], "%d", (&IntContent));
             * ((tPPi32 *) (sTokenMap[MapIdx].pPlace)) = IntContent;
        }
        else if (sTokenMap[MapIdx].nType == 2)
        {
              sscanf ((const char *)items[i+2], "%d", &IntContent);
              * ((tPPi16 *) (sTokenMap[MapIdx].pPlace)) =
                                                        (tPPi16)IntContent;
        }
        else if (sTokenMap[MapIdx].nType == 1)
        {
             sscanf ((const char *)items[i+2], "%d", &IntContent);
             * ((tPPi8 *) (sTokenMap[MapIdx].pPlace)) =
                                                          (tPPi8)IntContent;
        }
        else
        {
            printf("\nParameter Name '%s' not recognized...  \n", items[i]);
            fflush(stdout);
        }
    }
    return 0 ;
}  /* ParseContent () */

/**
********************************************************************************
 *  @fn     GetConfigFileContent
 *
 *  @desc   Reads the configuration file content in a buffer and returns the
 *          address
 *          .
 *  @param  *fname :File pointer of config file.
 *
 *  @return One line of data from the config file other wise zero.
 *******************************************************************************
 */
static char *GetConfigFileContent (FILE *fname)
{
    tPPi32 FileSize;
    /*------------------------------------------------------------------------*/
    /* Check for the size of the file by going to end and finding the position*/
    /* of the file                                                            */
    /*------------------------------------------------------------------------*/
    if (0 != fseek (fname, 0, SEEK_END))
    {
        return 0;
    }
    FileSize = ftell (fname);
    /*------------------------------------------------------------------------*/
    /* If the file size is greater than 20K or no content in file return      */
    /*------------------------------------------------------------------------*/
    if (FileSize < 0 || FileSize >= 20000)
    {
        return 0;
    }
    /*------------------------------------------------------------------------*/
    /* Seek to the start of the frame                                         */
    /*------------------------------------------------------------------------*/
    if (0 != fseek (fname, 0, SEEK_SET))
    {
        return 0;
    }
    /*------------------------------------------------------------------------*/
    /* Note that ftell() gives us the file size as the file system sees it.   */
    /* The actual file size, as reported by fread() below will be often       */
    /* smaller due to CR/LF to CR conversion and/or control characters after  */
    /* the dos EOF marker in the file.                                        */
    /*------------------------------------------------------------------------*/
    FileSize = fread (buf, 1, FileSize, fname);
    /*------------------------------------------------------------------------*/
    /* To end the string put end of string marker                             */
    /*------------------------------------------------------------------------*/
    buf[FileSize] = '\0';

    fclose (fname);

    return buf;
} /* GetConfigFileContent() */
/**
********************************************************************************
 *  @fn     readparamfile(IH265VDEC_Params *params, tPPi8 *configFile)
 *  @brief  Reads the entire param file contents into a global buffer,which is
 *          used for parsing and updates the params to given addresses.
 *
 *  @param[in]  params : pointer to the codec params interface structure
 *  @param[in]  configFile : Name of the configuration file with path
 *
 *  @return     0 - if successfully parsed all the elements in param file and
 *                  their  values read into the memory addresses given in
 *                  token mappign array.
 *             XDM_EFAIL - For any file read operation related errors or if
 *                  unknown parameter names are entered or if the parameter
 *                  file syntax is not in compliance with the below
 *                  implementation.
********************************************************************************
*/
tPPi32 readparamfile(tPPi8 *configFile)
{
  tPPi8 *FileBuffer = NULL;
  tPPi32  retVal ;
  FILE        *fConfigFile;
  /*--------------------------------------------------------------------------*/
  /*  Open Test Config File                                                   */
  /*--------------------------------------------------------------------------*/
  fConfigFile = fopen((const char *)configFile,"r");
  /*--------------------------------------------------------------------------*/
  /*  Perform file open error check.                                          */
  /*--------------------------------------------------------------------------*/
  if (!fConfigFile)
  {
    printf("Couldn't open Parameter Config file %s.\n",(char *)configFile);
    fflush(stdout);
    return EC_PP_FAILURE;
  }
  /*--------------------------------------------------------------------------*/
  /* read the content in a buffer                                             */
  /*--------------------------------------------------------------------------*/
  FileBuffer = (tPPi8*)GetConfigFileContent(fConfigFile);
  /*--------------------------------------------------------------------------*/
  /* if the buffer address is NULL then return error                          */
  /*--------------------------------------------------------------------------*/
  if(FileBuffer)
  {
    /*------------------------------------------------------------------------*/
    /* Parse every string into items and group them into triplets.            */
    /* Decode these ordered triplets into correspondign indices in the global */
    /* Token Map arrray provided by the user.                                 */
    /*------------------------------------------------------------------------*/
    retVal  =
       ParseContent((char *)FileBuffer,strlen((const char *)FileBuffer));

    return retVal;
  }
  else
  {
    return EC_PP_FAILURE;
  }
  /*--------------------------------------------------------------------------*/
  /*    Close Config Parameter File.                                          */
  /*--------------------------------------------------------------------------*/
  fclose(fConfigFile);
}/* readparamfile */

/*----------------------------------------------------------------------------*/
/* Error strings which are mapped to codec errors                             */
/* Please refer User guide for more details on error strings                  */
/*----------------------------------------------------------------------------*/
static tH265_sErrorMap gErrorStrings[32] =
{
  (tPPi8 *)"PP_H265_ERR_UNSUPPORTED = 1 \0",
  (tPPi8 *)"PP_H265_ERR_NO_NALU_BEGIN_FOUND = 2\0",
  (tPPi8 *)"PP_H265_ERR_NO_NALU_END_FOUND = 3 \0",
  (tPPi8 *)"PP_H265_ERR_INVALID_NAL_UNIT_TYPE = 4\0",
  (tPPi8 *)"PP_H265_ERR_INSUFFICIENT_BUFFER = 5\0",
  (tPPi8 *)"PP_H265_ERR_DATA_SYNC = 6 \0",
  (tPPi8 *)"PP_H265_ERR_CRITICAL  = 7 \0",
  (tPPi8 *)"PP_H265_ERR_NO_VPS  = 8 \0",
  (tPPi8 *)"PP_H265_ERR_VPS = 9 \0",
  (tPPi8 *)"PP_H265_ERR_NO_SPS = 10 \0",
  (tPPi8 *)"PP_H265_ERR_SPS = 11 \0",
  (tPPi8 *)"PP_H265_ERR_NO_PPS = 12 \0",
  (tPPi8 *)"PP_H265_ERR_PPS = 13 \0",
  (tPPi8 *)"PP_H265_ERR_SLICELOSS = 14 \0",
  (tPPi8 *)"PP_H265_ERR_SLICEHDR = 15 \0",
  (tPPi8 *)"PP_H265_ERR_SLICEDATA = 16 \0",
  (tPPi8 *)"PP_H265_ERR_RANDOM_ACCESS_SKIP = 17 \0",
  (tPPi8 *)"PP_H265_ERR_REFPIC_NOT_FOUND  = 18\0",
  (tPPi8 *)"PP_H265_ERR_META_DATA = 19 \0"
};
/**
********************************************************************************
*  @fn     tPPResult gH265_Error_Report(FILE * fTraceFile,
*                                       tPPi32 nError)
*
*  @brief  This function will print error messages
*          This function will check for codec errors which are mapped to
*          errors in ppH265Dec_StatusParams structure and dumps them to a file
*          Returns EC_PP_FAILURE in case of fatal error
*
*  @param  fTraceFile  [IN]   File pointer to the trace log file
*
*  @param  nError      [IN]   Error message
*
*  @return     SC_PP_SUCCESS -  when there is no fatal error
*              EC_PP_FAILURE - when it is fatal error
********************************************************************************
*/
tPPResult gH265_Error_Report(FILE *fTraceFile,
                             tPPi32 nError)
{

    tPPi32 i;
    if(nError)
    {
        /*--------------------------------------------------------------------*/
        /* Loop through all the bits in error message and map to the global   */
        /* error string                                                       */
        /*--------------------------------------------------------------------*/
        for (i = 1; i < 32; i ++)
        {
            if (nError & (1 << i))
            {
                printf("ERROR: %s \n",  (char *)gErrorStrings[i-1].pErrorName);
                fflush(stdout);
#ifdef DUMP_ERROR
                fprintf(fTraceFile, "ERROR: %s \n",
                    (char *)gErrorStrings[i].pErrorName);
#endif
            }
        }
    }
    return SC_PP_SUCCESS;
}
