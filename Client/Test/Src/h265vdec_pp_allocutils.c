/** ===========================================================================
* @file h265vdec_pp_allocutilis.c
*
* @brief This file parses input config file
*
* ============================================================================
*
* Copyright � PathPartner Technology, 2012-2015
*
* This material,including documentation and  any related computer programs,is
* protected by copyright controlled by PathPartner Technology. All rights are
* reserved.
*
* ============================================================================
*
* <b> REVISION HISTORY </b>
* @n  ================  @n
* @version 0.1 : 10-Oct-2012 : Praveen GB : Initial Code
* @version 0.2 : 02-July-2014 : Karthik SM : Added config file parameters
* $RevLog$
*
*=============================================================================
*/

/*******************************************************************************
*   INCLUDE FILES
*******************************************************************************/

#include "exp_pp_h265decoder.h"
#include "h265vdec_pp_allocutils.h"

/*******************************************************************************
*   FUNCTION DEFINITIONS
*******************************************************************************/

/**
******************************************************************************
* @fn MEMMGR_AllocMemoryRequirements (tPPQueryMemRecords  *apQueryMemRecords,
*                                         tPPu32 aNumMemTabEntries)
*
*
* @brief This function is a utility function used to allocate memory
*         as per the modules request
*
*
* @param apQueryMemRecords [IN/OUT] Pointer to memory query records
*
* @param aNumMemTabEntries [OUT] Number of Mem Tab Entries
*
* @return Status code - SC_PP_SUCCESS       : if success
*                       EC_PP_OUT_OF_MEMORY : insufficient memory status
******************************************************************************
*/
tPPResult MEMMGR_AllocMemoryRequirements (tPPQueryMemRecords  \
                                          *apQueryMemRecords,
                                          tPPu32 aNumMemTabEntries)
{
    tPPu32 nMemTabIdx = 0;

    for(nMemTabIdx = 0; nMemTabIdx < aNumMemTabEntries; nMemTabIdx++)
    {
        apQueryMemRecords[nMemTabIdx].allotedHandle =
            (tPPu8 *) malloc(apQueryMemRecords[nMemTabIdx].reqSize);

        if (apQueryMemRecords[nMemTabIdx].allotedHandle == NULL)
        {
            return EC_PP_OUT_OF_MEMORY;
        }
    }

    return SC_PP_SUCCESS;
}


/**
******************************************************************************
* @fn MEMMGR_DeAllocMemory (tPPBaseDecoder *apBase)
*
* @brief This function destroys H.265 video decoder object
*
* @param appBase        [IN] Pointer to decoder object
*
* @return Status code - SC_PP_SUCCESS if success, or the following error codes
*                       EC_PP_FAILURE : General failure
*
******************************************************************************
*/
tPPResult MEMMGR_DeAllocMemory (tPPQueryMemRecords  *apQueryMemRecords)
{
    tPPi32 nIndex;

    if(apQueryMemRecords == NULL)
    {
      return EC_PP_FAILURE;
    }

    for(nIndex = PP_H265DEC_MAX_MEMTAB-1; nIndex >= 0; nIndex--)
    {
        if(apQueryMemRecords[nIndex].allotedHandle != NULL)
        {
            free(apQueryMemRecords[nIndex].allotedHandle);
            apQueryMemRecords[nIndex].allotedHandle = NULL;
        }
    }

    return SC_PP_SUCCESS;
}/* MEMMGR_DeAllocMemory */

