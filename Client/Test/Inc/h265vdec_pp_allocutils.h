/** ===========================================================================
 * @file h265dec_allocutils.h
 *
 * @brief This file declares utility functions used for memory allocation as 
 *        part of test application.
 *
 * ============================================================================
 *
 * Copyright � PathPartner Technology, 2012-2015
 *
 * This material,including documentation and  any related computer programs,is
 * protected by copyright controlled by PathPartner Technology. All rights are
 * reserved.
 *
 * ============================================================================
 *
 * <b> REVISION HISTORY </b>
 * @n  ================  @n
 * @version 0.1 : 30-Jun-2012 :  Sunil K : Initial Code
 * $RevLog$
 * 
 *=============================================================================
 */

#ifndef _H265DEC_ALLOCUTILS_
#define _H265DEC_ALLOCUTILS_

/*******************************************************************************
*   INCLUDE FILES
*******************************************************************************/

#include "exp_pp_h265decoder.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define PP_H265DEC_MAX_MEMTAB   70

#ifdef __cplusplus
extern "C"
{
#endif
/*******************************************************************************
*   FUNCTION PROTOTYPES
*******************************************************************************/

/**
******************************************************************************
* @fn MEMMGR_AllocMemoryRequirements (tPPQueryMemRecords  *apQueryMemRecords,
*                                         tPPu32 aNumMemTabEntries)
*
*
* @brief This function is a utility function used to allocate memory
*         as per the modules request
*
*
* @param apQueryMemRecords [IN/OUT] Pointer to memory query records
*
* @param aNumMemTabEntries [OUT] Number of Mem Tab Entries
*
* @return Status code - SC_PP_SUCCESS       : if success
*                       EC_PP_OUT_OF_MEMORY : insufficient memory status
******************************************************************************
*/

extern tPPResult MEMMGR_AllocMemoryRequirements (
  tPPQueryMemRecords  *apQueryMemRecords, tPPu32 aNumMemTabEntries);

/**
******************************************************************************
* @fn MEMMGR_DeAllocMemory (tPPBaseDecoder *apBase)
*
* @brief This function destroys H.265 video decoder object
*
* @param appBase        [IN] Pointer to decoder object
*
* @return Status code - SC_PP_SUCCESS if success, or the following error codes
*                       EC_PP_FAILURE : General failure
*
******************************************************************************
*/
extern tPPResult MEMMGR_DeAllocMemory (tPPQueryMemRecords  *apQueryMemRecords);


#ifdef __cplusplus
}
#endif

#endif /*_EXP_PP_H265decODER_H_*/


