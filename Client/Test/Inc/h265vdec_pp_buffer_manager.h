/** ===========================================================================
 * @file h265dec_buffer_manager.h
 *
 * @brief This file contains implementation of all the API functions for
 *        PathPartner's H265 decoder.
 *
 * ============================================================================
 *
 * Copyright � PathPartner Technology, 2012-2015
 *
 * This material,including documentation and  any related computer programs,is
 * protected by copyright controlled by PathPartner Technology. All rights are
 * reserved.
 *
 * ============================================================================
 *
 * <b> REVISION HISTORY </b>
 * @n  ================  @n
 * @version 0.1 : 30-Jun-2012 :  Sunil K : Structure declaration,
 *                                            API changes,function additions
 * $RevLog$
 *
 *=============================================================================
 */


#ifndef BUFFMANAGER_H_
#define BUFFMANAGER_H_

/***************************************************************
*  INCLUDE FILES
****************************************************************/
#include "exp_pp_h265decoder.h"
#include <stdio.h>
#include <stdlib.h>

/*******************************************************************************
*   MACROS AND STRUCTURES
*******************************************************************************/

#define MAX_BUFF_ELEMENTS 17
#define DPB_MAXLUMAPS 8912896  /*Value for level 5.2 from Table A-1*/
#define MIN(x,y) ((x < y) ? x : y)

/**
 *  @brief      Status of the buffer elements.
 */
typedef enum {
    BUFFMGR_FREE = 0,
    BUFFMGR_USED = 1
} BUFFMGR_BufferStatus;

/**
 *  @brief      Definition of buffer element in the buffer array.
 *            every time a new buffer is requested buffer manager
 *            module returns a pointer to one of the elements
 *            defined below from the buffer array
 */
typedef struct BuffEle
{
    /** Unique Id which identifies the buffer element */
    tPPu32 bufId;
    /**
     * Status of the buffer element: can be either free for allocation or
     * held by the codec and cannot be allocated
     */
    BUFFMGR_BufferStatus bufStatus;
    /** Size of buffer members held by this buffer element in bytes */
    tPPu32 bufSize[3];
    /** buffer members which contain the address of the actual buffers
     * represented by this buffer element */
    tPPu8 *buf[3];
} BUFFMGR_buffEle;

typedef BUFFMGR_buffEle* BUFFMGR_buffEleHandle;

extern BUFFMGR_buffEle buffArray[MAX_BUFF_ELEMENTS];

/*******************************************************************************
*   FUNCTION PROTOTYPES
*******************************************************************************/

/**
* @fn BUFFMGR_Init()
* @brief The BUFFMGR_Init function is called by the test application to
*        initialise the global buffer element array to default and to allocate
*        required number of memory data for reference and output buffers.
*        The maximum required dpb size is defined by the supported profile &
*        level.
*/
extern tPPi32 BUFFMGR_Init(tPPu32 totBufSize);

/**
* @fn BUFFMGR_ReInit()
* @brief The BUFFMGR_ReInit function allocates global luma and chroma buffers
*        and allocates entire space to first element. This element will be used
*        in first frame decode. After the picture's height and width and its
*        luma and chroma buffer requirements are obtained the global luma and 
*        chroma buffers are re-initialised to other elements in the buffer arary
*/
extern tPPi32 BUFFMGR_ReInit(tPPu32 lumaOneFrameBufSize,
                             tPPu32 chromaOneFrameBufSize);
/**
* @fn BUFFMGR_GetFreeBuffer()
* @brief The BUFFMGR_GetFreeBuffer function searches for a free buffer in the
*        global buffer array and returns the address of that element. Incase
*        if none of the elements are free then it returns NULL
*/
extern BUFFMGR_buffEleHandle BUFFMGR_GetFreeBuffer(void);

/**
*@fn BUFFMGR_ReleaseBuffer(buffId)
*@brief The BUFFMGR_ReleaseBuffer function takes an array of buffer-ids
*        which are released by the test-app. "0" is not a valid buffer Id
*        hence this function keeps moving until it encounters a buffer Id
*        as zero or it hits the MAX_BUFF_ELEMENTS
*/
extern void BUFFMGR_ReleaseBuffer(tPPi32 bufffId[]);

/**
*@fn BUFFMGR_DeInit()
*@brief        The BUFFMGR_DeInit function releases all memory allocated by 
*              buffer manager.
*/
extern void BUFFMGR_DeInit(void);

#endif /* BUFFMANAGER_H_ */

