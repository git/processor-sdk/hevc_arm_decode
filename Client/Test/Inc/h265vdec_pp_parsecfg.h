/** ===========================================================================
* @file h265enc_config.h
*
* @brief This file contains structures and function declaration required
*        to parse the file
* 
* ============================================================================
*
* Copyright � PathPartner Technology, 2012-2015
*
* This material,including documentation and  any related computer programs,is
* protected by copyright controlled by PathPartner Technology. All rights are
* reserved.
*
* ============================================================================
*
* <b> REVISION HISTORY </b>
* @n  ================  @n
* @version 0.1 : 10-Oct-2012 : Praveen GB : Initial Code
* @version 0.2 : 02-July-2014 : Karthik SM : Added config file parameters 
* $RevLog$
*
*=============================================================================
*/

#ifndef _H265DEC_PARSECFG_H_
#define _H265DEC_PARSECFG_H_

/***************************************************************
*  INCLUDE FILES
****************************************************************/

#include "exp_pp_h265decoder.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*******************************************************************************
*   MACROS AND STRUCTURES
*******************************************************************************/

/*#define DUMP_ERROR*/

#define MAX_ITEMS_TO_PARSE 256
typedef struct ppH265Dec_ConfigParams tPP_H265_ConfigParams;

/** Create parameter structure for decoder
 *  @brief Structure containing attributes of the decoder object to be instanced
 *  @ingroup H265_DECODER
 */
struct ppH265Dec_ConfigParams
{
    /* Max width*/
    tPPi32  nMaxWidth;
    /* Max height */
    tPPi32  nMaxHeight;
    /* Number of CPUs */
    tPPi32  nNumThreads;
    /* Number of frames to decode */
    tPPi32  nNumFramesToDecode;

    tPPi8 nInputFile[FILE_NAME_SIZE];
    tPPi8 nOutputFile[FILE_NAME_SIZE];
    tPPi8 nRefFile[FILE_NAME_SIZE];
    tPPi32 nInputLowDelayMode;
    tPPi32 nOutputLowDelayMode;
    tPPi32 nNumCTURows;
    tPPi32 nMetadataType;
    tPPi32 nDecodeHeader;
};
typedef struct H265CfgParseTokenMap tH265CfgParseTokenMap;
struct H265CfgParseTokenMap{
  tPPi8     *pTokenName;
  void      *pPlace;
  tPPi32    nType;
};
 /**
 *******************************************************************************
 *  @struct sTokenMapping
 *  @brief  Token Mapping structure for Error reporting
 *          This structure contains error reporting strings which are mapped to
 *          Codec errors
 *
 *  @param  pErrorName : Pointer to the error string
 *
 *******************************************************************************
*/
typedef struct H265_ErrorMap tH265_sErrorMap;
struct H265_ErrorMap{
  tPPi8 *pErrorName;
};


tPPResult gParseConfigFile(FILE                  *afpCfgFile,
                           tPP_H265_CreateParams *apH265_CfgParams);

/*******************************************************************************
*   FUNCTION PROTOTYPES
*******************************************************************************/

/**
********************************************************************************
 *  @fn     readparamfile(IH265VDEC_Params *params, tPPi8 *configFile)
 *  @brief  Reads the entire param file contents into a global buffer,which is
 *          used for parsing and updates the params to given addresses.
 *
 *  @param[in]  params : pointer to the codec params interface structure
 *  @param[in]  configFile : Name of the configuration file with path
 *
 *  @return     0 - if successfully parsed all the elements in param file and
 *                  their  values read into the memory addresses given in
 *                  token mappign array.
 *             XDM_EFAIL - For any file read operation related errors or if
 *                  unknown parameter names are entered or if the parameter
 *                  file syntax is not in compliance with the below
 *                  implementation.
********************************************************************************
*/
tPPi32 readparamfile(tPPi8 *configFile);
/**
********************************************************************************
*  @fn     tPPResult gH265_Error_Report(FILE * fTraceFile,
*                                       tPPi32 nError)
*
*  @brief  This function will print error messages
*          This function will check for codec errors which are mapped to
*          errors in ppH265Dec_StatusParams structure and dumps them to a file
*          Returns EC_PP_FAILURE in case of fatal error
*
*  @param  fTraceFile [IN]   File pointer to the trace log file
*
*  @param  nError      [IN]   Error message
*
*  @return     SC_PP_SUCCESS -  when there is no fatal error
*              EC_PP_FAILURE - when it is fatal error
********************************************************************************
*/
tPPResult gH265_Error_Report(FILE *fTraceFile,
                             tPPi32 nError);
#endif

